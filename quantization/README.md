# Challenges

- Behaviour around zero matters if the quantized signal is integrated within a digital PID controller (or some other control scheme containing an integrator). 

# Literature

- Wikipedia contributors. (2021, January 4). Quantization (signal processing). In Wikipedia, The Free Encyclopedia. Retrieved 22:24, January 23, 2021, from https://en.wikipedia.org/w/index.php?title=Quantization_(signal_processing)&oldid=998162670
- Pelgrom, M. J. (2013). Analog-to-digital conversion. In Analog-to-Digital Conversion (pp. 325-418). Springer, New York, NY. [Book webpage at the publisher's site](https://www.springer.com/gp/book/9781461413714).
- Widrow, B., & Kollár, I. (2008). Quantization noise. Cambridge University Press. [Book webpage at the publisher's site](https://doi.org/10.1017/CBO9780511754661).