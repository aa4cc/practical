# Practical aspects of control design and implementation

Various practical issues upon which we bumped across while designing and implementing control systems that were not covered in (introductory) control textbooks and academic courses. Later will possibly be turned into a self-paced online course.

A preliminary set of topics is below, but everyone (not only the members of the AA4CC group) can feel free to propose a topic as an Issue here at gitlab repository.

- Sampling rate selection
- Quantization of signals
- Anti-aliasing filtering
- Feedforward control (both from reference and measured disturbance)
- Input (or reference) shaping
- Cascade control design (DC motor control case study)
- (Motor) current control using PWM and H-bridges
- Low speed estimation from incremental encoders
- Software implementation of a digital PID controller
- Software Implementation of a general digital feedback controller in the state space form
- Stochastic error models of common sensors (MEMS gyros, accelerometers, ...)
- Generating code from Simulink
- ... 